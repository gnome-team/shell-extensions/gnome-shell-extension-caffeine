Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gnome-shell-extensions-caffeine
Source: https://github.com/eonpatapon/gnome-shell-extension-caffeine

Files: *
Copyright:
 © 2012-2016 Jean-Philippe Braun
 © 2012 Lutz
 © 2013-2014 Junior Polegato
 © 2013 Demetri0
 © 2013 Márcio Almada
 © 2013 RobberPhex
 © 2014 Rafael Ferraira
 © 2014 Roman Spirgi
 © 2015 Boyuan Yang
 © 2015 Cristian Beroiza
 © 2015 Dušan Kazik
 © 2017 Giuseppe Pignataro
 © 2017 Hüseyin Karacabey
 © 2022-2024 Pakaoraki
License: GPL-2+ and GPL-3+

Files: lint/eslintrc-gjs.yml
Copyright:
 2018 Claudio André
License: Expat or LGPL-2+

Files: debian/*
Copyright:
 © 2014-2015 Tobias Frost
 © 2016 Simon McVittie
License: GPL-2+

License: Expat
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    .
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    .
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 Caffeine is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 Caffeine is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License along
 with Caffeine. If not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 SPDX license expression LGPL-2.0-or-later refers to the GNU Library General
 Public License, version 2.0 or any later version. On a Debian system, see
 /usr/share/common-licenses/LGPL-2 for a copy of this license.
